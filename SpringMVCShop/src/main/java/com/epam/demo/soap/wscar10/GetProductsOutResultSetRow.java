//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.09.10 at 05:20:52 PM SAMT 
//


package com.epam.demo.soap.wscar10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetProductsOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetProductsOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_brand" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_category" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_product" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetProductsOutResultSetRow", propOrder = {
    "name",
    "price",
    "amount",
    "idBrand",
    "idCategory",
    "idProduct"
})
public class GetProductsOutResultSetRow {

    @XmlElement(required = true)
    protected String name;
    protected int price;
    protected int amount;
    @XmlElement(name = "id_brand")
    protected int idBrand;
    @XmlElement(name = "id_category")
    protected int idCategory;
    @XmlElement(name = "id_product")
    protected int idProduct;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the price property.
     * 
     */
    public int getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     */
    public void setPrice(int value) {
        this.price = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAmount(int value) {
        this.amount = value;
    }

    /**
     * Gets the value of the idBrand property.
     * 
     */
    public int getIdBrand() {
        return idBrand;
    }

    /**
     * Sets the value of the idBrand property.
     * 
     */
    public void setIdBrand(int value) {
        this.idBrand = value;
    }

    /**
     * Gets the value of the idCategory property.
     * 
     */
    public int getIdCategory() {
        return idCategory;
    }

    /**
     * Sets the value of the idCategory property.
     * 
     */
    public void setIdCategory(int value) {
        this.idCategory = value;
    }

    /**
     * Gets the value of the idProduct property.
     * 
     */
    public int getIdProduct() {
        return idProduct;
    }

    /**
     * Sets the value of the idProduct property.
     * 
     */
    public void setIdProduct(int value) {
        this.idProduct = value;
    }

}
