package com.shop.DAO;

import com.shop.domain.Cart;
import com.shop.domain.CartHasProduct;
import com.shop.domain.Product;
import com.shop.domain.User;
import com.shop.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Transactional
public class CartDAO {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UserManager userManager;
    public List<Product> getrestCart() {
        List<Product> list1 = new ArrayList<>();
        return list1 = em.createQuery("select product from Product product").getResultList();
    }

    public List<Product> getCart() {
        List<Product> list1 = new ArrayList<>();
        try {
            int usID = userManager.getUser().getId();
            Cart cart = (Cart) em.createQuery("select cart from Cart cart where cart.userId = :id_user").setParameter("id_user", usID).getSingleResult();
            List<CartHasProduct> list = em.createQuery("select cart_has_product from CartHasProduct cart_has_product where cart_has_product.idCart = :id_user").setParameter("id_user", cart.getId()).getResultList();
            for (CartHasProduct cartHasProduct : list) {
                int id = cartHasProduct.getIdProduct();
                Product product = (Product) em.createQuery("select product from Product product where product.id = :id_user").setParameter("id_user", id).getSingleResult();
                list1.add(product);
            }
        } catch (NullPointerException e) {
        }
        return list1;
    }

    public void delete(String id) {
        try {
            int usID = userManager.getUser().getId();
            Cart cart = (Cart) em.createQuery("select cart from Cart cart where cart.userId = :id_user").setParameter("id_user", usID).getSingleResult();
            List<CartHasProduct> list = new ArrayList<>();
            list = em.createQuery("select cart_has_product from CartHasProduct cart_has_product where cart_has_product.idCart = :idCart and cart_has_product.idProduct = :idProduct").setParameter("idCart", cart.getId()).setParameter("idProduct", Integer.parseInt(id)).getResultList();
            CartHasProduct cartHasProduct = list.get(0);
            em.remove(em.contains(cartHasProduct) ? cartHasProduct : em.merge(cartHasProduct));
        } catch (NullPointerException e) {
        }
    }

    public void add(String id) {
        try {
            int usID = userManager.getUser().getId();
            Cart cart = (Cart) em.createQuery("select cart from Cart cart where cart.userId = :id_user").setParameter("id_user", usID).getSingleResult();
            CartHasProduct cartHasProduct = new CartHasProduct(cart.getId(), Integer.parseInt(id));
            em.persist(cartHasProduct);
        } catch (NullPointerException e) {
        }
    }
}
