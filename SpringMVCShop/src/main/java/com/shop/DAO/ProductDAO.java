package com.shop.DAO;

import com.shop.domain.Product;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
@Transactional
public class ProductDAO {

    @PersistenceContext
    private EntityManager em;

    public List<Product> getProducts(){
        return em.createQuery("select product from Product product").getResultList();
    }

}
