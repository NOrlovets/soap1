package com.shop.DAO;


import com.shop.domain.Cart;
import com.shop.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Component
@Transactional
public class UserDAO {

    @PersistenceContext
    private EntityManager em;

    public void create(User user) {
        user.setIdRole(5);
        em.persist(user);
        User foundUser = (User) em.createQuery("select user from User user where user.login = :login").setParameter("login", user.getLogin()).getSingleResult();
        em.persist(new Cart(0, 0, foundUser.getId()));
    }

    public User auth(User user) {
        User foundUser;
        try {
            foundUser = (User) em.createQuery("select user from User user where user.login = :login").setParameter("login", user.getLogin()).getSingleResult();
        } catch (NoResultException e) {
            foundUser = null;
        }
        return foundUser;
    }

}
