package com.shop.controller;

import com.shop.DAO.CartDAO;
import com.shop.DAO.UserDAO;
import com.shop.domain.User;
import com.shop.manager.UserManager;
//import com.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;


@Controller
public class AuthController {

    @Autowired
    private UserManager userManager;

    @Autowired
    private UserDAO userDAO;

    @GetMapping("/auth")
    public String authGet() {
        return "auth";
    }

    @PostMapping("/auth")
    public String authPost(@Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "auth";
        }
        User foundUser = userDAO.auth(user);
        if (foundUser != null) {
            if (foundUser.getPassword().equals(user.getPassword())) {
                userManager.setUser(foundUser);
            }
        }
        return "redirect:/main";
    }

}
