package com.shop.controller;

import com.shop.DAO.ProductDAO;
import com.shop.domain.CartHasProduct;
import com.shop.domain.Product;
import com.shop.manager.UserManager;
import com.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private UserManager userManager;

    @GetMapping("/main")
    public String greeting(Model model) {
        model.addAttribute("name", userManager.getUser().getName());
        //model.addAttribute("list", productDAO.getProducts());
        //productService.findAll().forEach(System.out::println);
        model.addAttribute("list", productService.findAll());
        return "main";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/auth";
    }

}
