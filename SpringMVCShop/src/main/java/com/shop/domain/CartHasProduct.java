package com.shop.domain;

import javax.persistence.*;

@Entity
@Table(name = "cart_has_product")
public class CartHasProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_chp")
    private int idChp;
    @Column(name = "id_cart")
    private int idCart;
    @Column(name = "id_product")
    private int idProduct;

    public CartHasProduct(){}

    public CartHasProduct(int idCart, int idProduct) {
        this.idCart = idCart;
        this.idProduct = idProduct;
    }

    public CartHasProduct(int idChp, int idCart, int idProduct) {
        this.idChp = idChp;
        this.idCart = idCart;
        this.idProduct = idProduct;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public int getIdCart() {
        return idCart;
    }

    public void setIdCart(int idCart) {
        this.idCart = idCart;
    }

}
