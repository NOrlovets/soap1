package com.shop.domain;

import javax.persistence.*;

@Entity
@Table(name = "category")
public class Category {

    @Id
    @Column(name="id_category")
    private int id;
    @Column(name="name")
    private String category;

    Category(){}


    public Category(int id, String category) {
        this.id = id;
        this.category = category;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
