package com.shop.endpoint;


import com.epam.demo.soap.wscar10.*;
import com.shop.domain.Product;
import com.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class ProductsEndpoint {
    private static final String NAMESPACE_URI = "http://epam.com/demo/soap/WSCar10";

    private ProductRepository repository;

    @Autowired
    public ProductsEndpoint(ProductRepository repository) {
        this.repository = repository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetProductsRequest")
    @ResponsePayload
    public GetProductsResponse getProducts(@RequestPayload GetProductsRequest request) {
        String brand = request.getInParams().getBrandName();
        List<Product> carsByBrand;
        if (brand.equals("1")) {
            carsByBrand = repository.findProducts();
        } else {
            carsByBrand = repository.getAllProducts();
        }
//        for (Product product : carsByBrand) {
//            System.out.println(product.getName());
//        }
        GetProductsResponse response = new GetProductsResponse();
        GetProductsOutParams outParams = new GetProductsOutParams();
        GetProductsOutResultSet resultSet = new GetProductsOutResultSet();
        carsByBrand.stream().map(this::mapToRow).forEach(resultSet.getResultSetRow()::add);
        outParams.setResultSet(resultSet);
        response.setOutParams(outParams);
        return response;
    }

    private GetProductsOutResultSetRow mapToRow(Product car) {
        GetProductsOutResultSetRow resultSetRow = new GetProductsOutResultSetRow();
        resultSetRow.setPrice(car.getPrice());
        resultSetRow.setName(car.getName());
        resultSetRow.setAmount(car.getAmount());
        resultSetRow.setIdBrand(car.getIdBrand());
        resultSetRow.setIdCategory(car.getIdCategory());
        resultSetRow.setIdProduct(car.getId());
        return resultSetRow;
    }
}
