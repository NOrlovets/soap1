package com.shop.repository;

import com.shop.domain.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> getAllProducts();
    List<Product> findProducts();
}
