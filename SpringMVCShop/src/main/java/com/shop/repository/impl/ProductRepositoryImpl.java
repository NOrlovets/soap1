package com.shop.repository.impl;

import com.shop.domain.Product;
import com.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@Transactional
public class ProductRepositoryImpl implements ProductRepository {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public ProductRepositoryImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Cacheable("products")
    public List<Product> getAllProducts() {
        return jdbcTemplate.query("SELECT * FROM product", this::mapRow);
    }

    public List<Product> findProducts() {
        return jdbcTemplate.query("SELECT * FROM product WHERE id_category='1'", this::mapRow);
    }

    private Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt("id_product"));
        product.setName(rs.getString("name"));
        product.setPrice(rs.getInt("price"));
        product.setAmount(rs.getInt("amount"));
        product.setIdBrand(rs.getInt("id_brand"));
        product.setIdCategory(rs.getInt("id_category"));
        return product;
    }

}
